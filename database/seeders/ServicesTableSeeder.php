<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use \App\Models\Service;


class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $defined_elements = [
            [
                'name'  => 'ideabot'
            ],
            [
                'name'  => 'card'
            ],
        ];

        foreach($defined_elements as $defined_element) {
            $service = new Service();
            $service->name = $defined_element['name'];
            $service->save();
        }
    }
}

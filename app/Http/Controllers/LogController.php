<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Log;
use Carbon\Carbon;
use DateTime;
use App\Repositories\Log\LogRepository;



class LogController extends Controller
{

    private $log;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(LogRepository $log)
    {
        $this->log = $log;
    }


    public function getAllLogs()
    {
        return $this->log->getAll();
    }

// ==========================================================================================================================//

    //the OLD CODEEEE


    
    /**
     * Store new log or update check out.
     *
     * @param  Request  $request
     * @return Response
     */
    public function check(Request $request)
    {
        $log = new Log;
        return $log->checkLog($request->all());
    }


    public function getCurrentStatus(Request $request)
    {
        $logs = Log::where('cultiv_id', $request->cultiv_id)->get();
        $statuss = [];
        foreach($logs as $log){

             $diff =  Carbon::parse($log->check_out)->diffInMinutes(Carbon::parse($log->check_in));
             $diff =  gmdate("H:i", ($diff * 60));

            $statuss[] =  'You have '.$log->status.' '.$log->location.' at '. date('h:i a', strtotime($log->check_out)).' ('.$diff.')';


        }
        return $statuss;
        
        return response()->json(['message' => 'You have checked in '.$this->payload['location']], 201);
        // return $log->logStatus($request->cultiv_id);
    }


    public function showLog($id)
    {
        return Log::find($id);
    }
}

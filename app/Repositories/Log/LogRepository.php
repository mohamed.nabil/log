<?php


namespace App\Repositories\Log;


interface LogRepository
{
	public function getAll();

	public function getById($id);

}
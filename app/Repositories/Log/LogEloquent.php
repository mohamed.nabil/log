<?php


namespace App\Repositories\Log;
use App\Models\Log;


class LogEloquent implements LogRepository
{

	private $model;

	public function __construct(Log $model)
	{
		$this->model = $model;
	}



	public function getAll()
	{
		return $this->model->all();
	}

	public function getById($id)
	{
		return $this->model->findById($id);
	}

}
<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Log extends Model
{

    protected $guarded = [];
    private $payload;
    private $entity;
	private $latest_possible_time = '23:59:00';
    

    // public function getCreatedAtAttribute($date)
    // {
    //     return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');
    // }


    public function checkLog($payload)
    {
    	$this->payload = $payload;
		return $this->checkedIn() ? $this->checkedOut() : $this->createNewLog();
	}


	public function checkedIn()
    {
    	return $this->entity = Log::where('cultiv_id', $this->payload['cultiv_id'])
    				->where('location', $this->payload['location'])
    				->whereDate('created_at', Carbon::today())
    				->latest()->first();
    }


    public function checkedOut()
    {
    	return $this->entity->check_out == $this->latest_possible_time ? $this->updateCheckOut() : $this->createNewLog();
    }


    public function createNewLog()
    {
        Log::create([
            'cultiv_id' => $this->payload['cultiv_id'],
            'location' => $this->payload['location'],
            'service_id' => $this->payload['service_id'],
            'check_in' => Carbon::now()->timezone('Africa/Cairo')->format('H:i'),
            // 'check_out' => $this->latest_possible_time,
         ]);
        return response()->json(['message' => 'You have checked in '.$this->payload['location']], 201);
    }



    public function updateCheckOut()
    {
        $this->entity->check_out = Carbon::now()->timezone('Africa/Cairo')->format('H:i');
        $this->entity->status = 'checked out';
        $this->entity->save();
        return response()->json(['message' => 'You have checked out!'], 201);
    }
}